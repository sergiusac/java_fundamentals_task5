package epamjava.fundamentals.task5;

import epamjava.fundamentals.task5.creators.OrderCreator;
import epamjava.fundamentals.task5.exceptions.IncorrectOrderNumberException;
import epamjava.fundamentals.task5.exceptions.TooManyIngredientsException;
import epamjava.fundamentals.task5.exceptions.TooManyPizzasException;
import epamjava.fundamentals.task5.repos.IngredientRepo;

public class Main {
    public static void main(String[] args) {
        try {
            OrderCreator orderCreator1 = new OrderCreator();
            orderCreator1.setOrderNumber(10001);
            orderCreator1.setClientNumber(7717);

            // pizza #1
            int pizzaNumber1 = orderCreator1.addPizza();
            orderCreator1.setPizzaName(pizzaNumber1, "Margarita");
            orderCreator1.setPizzaCount(pizzaNumber1, 2);
            orderCreator1.setPizzaCalzone(pizzaNumber1, true);
            orderCreator1.addIngredient(pizzaNumber1, IngredientRepo.getAvailableIngredients().get(0));
            orderCreator1.addIngredient(pizzaNumber1, IngredientRepo.getAvailableIngredients().get(1));
            orderCreator1.addIngredient(pizzaNumber1, IngredientRepo.getAvailableIngredients().get(7));

            // pizza #2
            int pizzaNumber2 = orderCreator1.addPizza();
            orderCreator1.setPizzaName(pizzaNumber2, "Pepperoni");
            orderCreator1.setPizzaCount(pizzaNumber2, 3);
            orderCreator1.setPizzaCalzone(pizzaNumber2, false);
            orderCreator1.addIngredient(pizzaNumber2, IngredientRepo.getAvailableIngredients().get(1));
            orderCreator1.addIngredient(pizzaNumber2, IngredientRepo.getAvailableIngredients().get(6));

            System.out.println(orderCreator1.create().toString());
        } catch (TooManyPizzasException | IncorrectOrderNumberException | TooManyIngredientsException e) {
            System.err.println(e.getMessage());
        }

        try {
            OrderCreator orderCreator2 = new OrderCreator();
            orderCreator2.setOrderNumber(10002);
            orderCreator2.setClientNumber(4372);

            // pizza #3
            int pizzaNumber3 = orderCreator2.addPizza();
            orderCreator2.setPizzaName(pizzaNumber3, "Base ZZ");
            orderCreator2.setPizzaCalzone(pizzaNumber3, false);
            orderCreator2.setPizzaCount(pizzaNumber3, 12);

            System.out.println(orderCreator2.create().toString());
        } catch (TooManyPizzasException | IncorrectOrderNumberException e) {
            System.err.println(e.getMessage());
        }
    }
}
