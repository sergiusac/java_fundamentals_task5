package epamjava.fundamentals.task5.models;

public class Ingredient {
    private String name;
    private double price;

    public Ingredient() {
    }

    public Ingredient(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s\t%.2f €\n", getName(), getPrice()));

        return builder.toString();
    }
}
