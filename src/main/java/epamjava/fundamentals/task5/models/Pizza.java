package epamjava.fundamentals.task5.models;

import epamjava.fundamentals.task5.PriceCalculator;

import java.util.ArrayList;
import java.util.List;

public class Pizza {
    private String name;
    private List<Ingredient> ingredients;
    private boolean isCalzone;
    private int count;

    public Pizza() {
        ingredients = new ArrayList<>();
    }

    public Pizza(String name, List<Ingredient> ingredients, boolean isCalzone, int count) {
        this.name = name;
        this.ingredients = ingredients;
        this.isCalzone = isCalzone;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isCalzone() {
        return isCalzone;
    }

    public void setCalzone(boolean calzone) {
        isCalzone = calzone;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void addIngredient(Ingredient ingredient) {
        ingredients.add(ingredient);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("--------------------------------\n");
        builder.append(String.format("Name: %s\n", getName()));
        builder.append("--------------------------------\n");
        builder.append(String.format(
                "Pizza Base %s\t%.2f €\n",
                isCalzone() ? "(Calzone)" : "",
                PriceCalculator.calculatePizzaBasePrice(this)
        ));

        for (Ingredient ingredient : getIngredients()) {
            builder.append(ingredient.toString());
        }

        builder.append("--------------------------------\n");
        builder.append(String.format("Price: %.2f €\n", PriceCalculator.calculatePizzaPrice(this)));
        builder.append(String.format("Count: %d\n", getCount()));

        return builder.toString();
    }
}
