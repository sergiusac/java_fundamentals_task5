package epamjava.fundamentals.task5.models;

import epamjava.fundamentals.task5.PriceCalculator;

import java.time.LocalTime;
import java.util.List;

public class Order {
    private LocalTime time;
    private int orderNumber;
    private int clientNumber;
    private List<Pizza> pizzas;

    public Order() {
        time = LocalTime.now();
    }

    public Order(int orderNumber, int clientNumber, List<Pizza> pizzas) {
        time = LocalTime.now();
        this.orderNumber = orderNumber;
        this.clientNumber = clientNumber;
        this.pizzas = pizzas;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public String attributesOfPizza(int number) {
        Pizza pizza = pizzas.get(number);

        return String.format(
                "[%d; %d; %s; %d]",
                orderNumber,
                clientNumber,
                pizza.getName(),
                pizza.getCount()
        );
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("********************************\n");
        builder.append(String.format("Order: %d\n", getOrderNumber()));
        builder.append(String.format("Client: %d\n", getClientNumber()));

        for (Pizza pizza : getPizzas()) {
            builder.append(pizza.toString());
        }

        builder.append("--------------------------------\n");

        builder.append(String.format("Total Price: %.2f €\n", PriceCalculator.calculateOrderPrice(this)));
        builder.append("********************************\n");

        return builder.toString();
    }
}
