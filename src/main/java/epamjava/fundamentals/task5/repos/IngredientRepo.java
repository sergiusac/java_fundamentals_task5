package epamjava.fundamentals.task5.repos;

import epamjava.fundamentals.task5.models.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class IngredientRepo {

    private static List<Ingredient> availableIngredients = new ArrayList<>();
    static {
        availableIngredients.add(new Ingredient("Tomato Paste", 1));
        availableIngredients.add(new Ingredient("Cheese", 1));
        availableIngredients.add(new Ingredient("Salami", 1.5));
        availableIngredients.add(new Ingredient("Bacon", 1.2));
        availableIngredients.add(new Ingredient("Garlic", 0.3));
        availableIngredients.add(new Ingredient("Corn", 0.7));
        availableIngredients.add(new Ingredient("Pepperoni", 0.6));
        availableIngredients.add(new Ingredient("Olives", 0.5));
    }

    public static List<Ingredient> getAvailableIngredients() {
        return availableIngredients;
    }

}
