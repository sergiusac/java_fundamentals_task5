package epamjava.fundamentals.task5;

import epamjava.fundamentals.task5.models.Ingredient;
import epamjava.fundamentals.task5.models.Order;
import epamjava.fundamentals.task5.models.Pizza;

public class PriceCalculator {

    public final static double pizzaBasePrice = 1;
    public final static double pizzaCalzoneSurcharge = 0.5;

    public static double calculatePizzaBasePrice(Pizza pizza) {
        return  pizzaBasePrice + (pizza.isCalzone() ? pizzaCalzoneSurcharge : 0);
    }

    public static double calculatePizzaPrice(Pizza pizza) {
        double res = calculatePizzaBasePrice(pizza);

        for (Ingredient ingredient : pizza.getIngredients()) {
            res += ingredient.getPrice();
        }

        return res;
    }

    public static double calculateOrderPrice(Order order) {
        double res = 0;

        for (Pizza pizza : order.getPizzas()) {
            res += calculatePizzaPrice(pizza) * pizza.getCount();
        }

        return res;
    }
}
