package epamjava.fundamentals.task5.exceptions;

public class IncorrectOrderNumberException extends Exception {
    public IncorrectOrderNumberException() {
        super();
    }

    public IncorrectOrderNumberException(String message) {
        super(message);
    }

    public IncorrectOrderNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectOrderNumberException(Throwable cause) {
        super(cause);
    }

    protected IncorrectOrderNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
