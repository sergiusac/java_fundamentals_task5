package epamjava.fundamentals.task5.exceptions;

public class TooManyIngredientsException extends Exception {
    public TooManyIngredientsException() {
        super();
    }

    public TooManyIngredientsException(String message) {
        super(message);
    }

    public TooManyIngredientsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooManyIngredientsException(Throwable cause) {
        super(cause);
    }

    protected TooManyIngredientsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
