package epamjava.fundamentals.task5.exceptions;

public class TooManyPizzasException extends Exception {
    public TooManyPizzasException() {
        super();
    }

    public TooManyPizzasException(String message) {
        super(message);
    }

    public TooManyPizzasException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooManyPizzasException(Throwable cause) {
        super(cause);
    }

    protected TooManyPizzasException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
