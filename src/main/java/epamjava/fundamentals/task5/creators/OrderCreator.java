package epamjava.fundamentals.task5.creators;

import epamjava.fundamentals.task5.exceptions.IncorrectOrderNumberException;
import epamjava.fundamentals.task5.exceptions.TooManyIngredientsException;
import epamjava.fundamentals.task5.exceptions.TooManyPizzasException;
import epamjava.fundamentals.task5.models.Ingredient;
import epamjava.fundamentals.task5.models.Order;
import epamjava.fundamentals.task5.models.Pizza;
import epamjava.fundamentals.task5.repos.IngredientRepo;

import java.util.ArrayList;

public class OrderCreator {

    private Order order;

    public OrderCreator() {
        order = new Order();
        order.setPizzas(new ArrayList<>());
    }

    public void setOrderNumber(int orderNumber) throws IncorrectOrderNumberException {
        if (!checkOrderNumber(orderNumber)) {
            throw new IncorrectOrderNumberException("Order number must contain only five digits");
        }

        order.setOrderNumber(orderNumber);
    }

    public void setClientNumber(int clientNumber) {
        order.setClientNumber(clientNumber);
    }

    public int addPizza() {
        order.getPizzas().add(new Pizza());
        return order.getPizzas().size() - 1;
    }

    public void setPizzaName(int number, String name) {
        if (name == null || !checkPizzaName(name)) {
            name = order.getClientNumber() + "_" + order.getPizzas().size();
        }
        order.getPizzas().get(number).setName(name);
    }

    public void setPizzaCalzone(int number, boolean isCalzone) {
        order.getPizzas().get(number).setCalzone(isCalzone);
    }

    public void setPizzaCount(int number, int count) throws TooManyPizzasException {
        if (!checkPizzaCount(count)) {
            throw new TooManyPizzasException("Number of pizzas must be less of equal than 10");
        }
        order.getPizzas().get(number).setCount(count);
    }

    public void addIngredient(int number, Ingredient ingredient) throws TooManyIngredientsException {
        checkIngredient(order.getPizzas().get(number), ingredient);
        order.getPizzas().get(number).addIngredient(ingredient);
    }

    public Order create() {
        return order;
    }

    private boolean checkOrderNumber(int orderNumber) {
        return orderNumber >= 10000 && orderNumber <= 100000;
    }

    private boolean checkPizzaName(String name) {
        return name.length() >= 4 && name.length() <= 20;
    }

    private boolean checkPizzaCount(int count) {
        return count <= 10;
    }

    private void checkIngredient(Pizza pizza, Ingredient ingredient) throws TooManyIngredientsException {
        if (containsIngredient(pizza, ingredient)) {
            throw new TooManyIngredientsException("Pizza already contains the ingredient");
        }

        if (pizza.getIngredients().size() > IngredientRepo.getAvailableIngredients().size()) {
            throw new TooManyIngredientsException("Pizza is full");
        }
    }

    private boolean containsIngredient(Pizza pizza, Ingredient ingredient) {
        return pizza.getIngredients().contains(ingredient);
    }
}
